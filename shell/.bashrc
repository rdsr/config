alias ew=emacs
alias ec=emacsclient
alias e="emacs -nw"

#VISUAL=emacsclient; export VISUAL
#EDITOR=emacsclient; export EDITOR

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_141.jdk/Contents/Home
export PATH=$PATH:$JAVA_HOME/bin

export MAVEN_HOME="$HOME/setups/maven"
export MAVEN_OPTS="-Xms1024m -Xmx4096m -XX:PermSize=1024m"
export PATH=$MAVEN_HOME/bin:$PATH

export PATH=/Users/rratti/bin:$PATH:/opt/local/bin

source ~/bin/git_autocomplete

export GITAWAREPROMPT=~/.bash/git-aware-prompt
source "${GITAWAREPROMPT}/main.sh"

export PS1="\u@\h \W \[$txtcyn\]\$git_branch\[$txtred\]\$git_dirty\[$txtrst\]\$ "
export EDITOR=ec
