;; RD's .emacs file
;; 8th September 2007

;; load-path settings

(add-to-list 'load-path (concat (getenv "HOME") "/.emacs.d/elpa/"))

(global-font-lock-mode t)
(setq font-lock-maximum-decoration t)
(fset 'yes-or-no-p 'y-or-n-p)
(setq woman-use-own-frame nil) ;; don't create new frame for manpages

;; ediff settings
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-horizontally)

;; enabling disabled commands.
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

(define-key global-map (kbd "RET") 'newline-and-indent)

;; handy aliases
(defalias 'fs'  toggle-fullscreen)
(defalias 'qr'  query-replace)
(defalias 'qrr' query-replace-regexp)
(defalias 'adf' file-cache-add-directory-using-find)
(defalias 'adl' file-cache-add-directory-using-locate)

;; more emacs realestate
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tabbar-mode) (tabbar-mode -1))


;; window resizing
(global-set-key (kbd "S-C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "S-C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "S-C-<down>") 'shrink-window)
(global-set-key (kbd "S-C-<up>") 'enlarge-window)

;; key bindings
(global-set-key "\C-x\C-k" 'kill-region)
(global-set-key "\C-x\C-q" 'quoted-insert)
(global-set-key "\C-xk" 'kill-this-buffer)
(global-set-key "\C-x;" 'uncomment-region)
(global-set-key "\C-w" 'backward-kill-word)
(global-set-key "\M-s" 'isearch-forward-regexp)
(global-set-key "\M-r" 'isearch-backward-reqexp)
;; enable C-x C-m sequence in place of M-x
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c\C-v" 'scroll-down-command)

;; eshell settings
(setq eshell-hist-ignoredups t)
(setq eshell-history-size 8192)
(setq eshell-save-history-on-exit t)
(setq eshell-cmpl-cycle-completions nil)
(setq eshell-scroll-to-bottom-on-output t)
(setq eshell-scroll-show-maximum-output t)
(setq eshell-cp-interactive-query t)
(setq eshell-ln-interactive-query t)
(setq eshell-mv-interactive-query t)
(setq eshell-rm-interactive-query t)
(setq eshell-mv-overwrite-files t)


(require 'shell-toggle-patched)
(autoload 'shell-toggle "shell-toggle"
  "Toggles between the shell buffer and whatever buffer you are editing."
  t)
(autoload 'shell-toggle-cd "shell-toggle"
  "Pops up a shell-buffer and insert a \"cd <file-dir>\" command." t)
(setq shell-toggle-launch-shell 'shell-toggle-eshell)
(define-key global-map "\C-cs" 'shell-toggle)
(define-key global-map "\C-cd" 'shell-toggle-cd)

;; smtp settings
;; requires external program: gnutls (for tls)
(setq send-mail-function 'smtpmail-send-it)
(setq smtpmail-smtp-server "smtp.gmail.com")
(setq smtpmail-smtp-service 587)
(setq smtpmail-auth-credentials
      '(("smtp.gmail.com" 587 "rdsr.me" nil)))
(setq smtpmail-starttls-credentials
      '(("smtp.gmail.com" 587 nil nil)))

;; open *help* in current frame
(setq special-display-regexps (remove "[ ]?\\*[hH]elp.*" special-display-regexps))

;; backup/autosave files in one place
(setq backup-directory-alist `((".*" . , temporary-file-directory)))
(setq auto-save-file-name-transforms `((".*" , temporary-file-directory t)))

;; mode line
;(set-face-background 'modeline-inactive "gray30")

;; disable file locking
(setq create-lockfiles nil)

;; org
(setq org-archive-location "/tmp/%s_archive::")
(org-babel-do-load-languages
 'org-babel-load-languages '((python . t)))


(require 'package)
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)


(require 'ivy)
(ivy-mode 1)
(setq ivy-use-virtual-buffers 'full)
(setq enable-recursive-minibuffers t)
(setq ivy-count-format "")
(setq ivy-re-builders-alist '((t . ivy--regex-ignore-order)))

;(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-x C-m") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)

(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)

(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
(define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
(define-key ivy-minibuffer-map (kbd "RET") #'ivy-alt-done)

(require 'fullscreen-mode)
(fullscreen-mode-fullscreen-toggle)

;; magit
(global-set-key (kbd "C-x g") 'magit-status)


(load-theme 'dracula)

;; Minor modes
(column-number-mode t)
(show-paren-mode t)


(load "utils.el")


(setenv "JAVA_HOME" "/System/Library/Frameworks/JavaVM.framework/Versions/Current")
(setenv "PATH" (concat "/Users/rratti/bin:"
		       (getenv "PATH")))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("ff7625ad8aa2615eae96d6b4469fcc7d3d20b2e1ebc63b761a349bebbb9d23cb" default)))
 '(package-selected-packages
   (quote
    (magit gradle-mode clojure-mode markdown-mode+ scala-mode dracula-theme kotlin-mode use-package ivy-todo fullscreen-mode counsel))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:height 142 :family "monaco")))))

(server-start)
