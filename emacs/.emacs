(require 'package)
(setq package-archives
      '(("melpa-stable" . "https://stable.melpa.org/packages/")
	("elpa"         . "https://elpa.gnu.org/packages/")        
        ("melpa"        . "https://melpa.org/packages/"))
      package-archive-priorities
      '(("melpa-stable" . 10)
        ("elpa"         . 5)
        ("melpa"        . 0)))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(add-to-list 'load-path (concat (getenv "HOME") "/.emacs.d/lib/"))

(use-package font-core
  :init
  (setq font-lock-maximum-decoration t)
  :config 
  (global-font-lock-mode t))

(use-package ediff
  :init
  (setq ediff-window-setup-function 'ediff-setup-windows-plain)
  (setq ediff-split-window-function 'split-window-horizontally))

(use-package files
  :init
  ;; backup/autosave files in one place
  (setq backup-directory-alist `((".*" . , temporary-file-directory)))
  (setq auto-save-file-name-transforms `((".*" , temporary-file-directory t)))
  ;; disable file locking
  (setq create-lockfiles nil))

(use-package simple
  ;; key bindings
  :bind
  (("\C-x\C-k" . kill-region)
   ("\C-x\C-q" . quoted-insert)
   ("\C-xk" . kill-this-buffer)
   ("\C-x;" . uncomment-region)
   ("\C-w" . backward-kill-word)
   ("\M-s" . isearch-forward-regexp)
   ("\M-r" . isearch-backward-reqexp)
   (["RET"] . newline-and-indent)
   ;; enable C-x C-m sequence in place of M-x
   ("\C-x\C-m" . execute-extended-command)   
   ("\C-c\C-v" . scroll-down-command))

  :config
  (column-number-mode t))

(use-package eshell
  :ensure t
  :init 
  (setq eshell-hist-ignoredups t)
  (setq eshell-history-size 8192)
  (setq eshell-save-history-on-exit t)
  (setq eshell-cmpl-cycle-completions nil)
  (setq eshell-scroll-to-bottom-on-output t)
  (setq eshell-scroll-show-maximum-output t)
  (setq eshell-cp-interactive-query t)
  (setq eshell-ln-interactive-query t)
  (setq eshell-mv-interactive-query t)
  (setq eshell-rm-interactive-query t)
  (setq eshell-mv-overwrite-files t))

(use-package shell-toggle-patched
  :commands
  (shell-toggle shell-toggle-cd)

  :bind
  (("\C-cs" . shell-toggle)
   ("\C-cd" . shell-toggle-cd))

  :init
  (setq shell-toggle-launch-shell 'shell-toggle-eshell))

;; org
(use-package org-mode
  :disabled
  :init
  (setq org-archive-location "/tmp/%s_archive::")
  
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages '((python . t))))

(use-package ivy
  :ensure t
  :init
  (setq ivy-use-virtual-buffers 'full)
  ;(setq enable-recursive-minibuffers t)
  ;(setq ivy-count-format "")
  ;(setq ivy-re-builders-alist '((t . ivy--regex-ignore-order)))

  :bind
  (;("\C-s" . 'swiper)
   ("C-x C-m" . counsel-M-x)
   ("C-x C-f" . counsel-find-file)
   ("<f1> f" . counsel-describe-function)
   ("<f1> v" . counsel-describe-variable)
   ("<f1> l" . counsel-find-library)
   ("<f2> i" . counsel-info-lookup-symbol)
   ("<f2> u" . counsel-unicode-char)
   ("C-c g" . counsel-git)
   ("C-c j" . counsel-git-grep)
   ("C-c k" . counsel-ag)
   ("C-x l" . counsel-locate)
   ("C-c C-r" . ivy-resume)
   ;("C-r" . counsel-expression-history)
   ;("<RET>" . ivy-alt-done)
)
  :config
  (ivy-mode 1))

(use-package magit
  :ensure t
  :config
  :bind 
  (("C-x g". magit-status)))

(use-package company 
  :ensure t
  :bind 
  (("TAB" . company-indent-or-complete-common))
  :config
  (add-hook 'after-init-hook 'global-company-mode))

(use-package cider
  :ensure t
  :config
  (add-hook 'clojure-mode-hook #'cider-mode)
  (add-hook 'cider-mode-hook
	    '(lambda ()
	       (local-set-key
		(kbd "RET")
		'newline-and-indent)))
  (add-hook 'cider-repl-mode-hook #'company-mode)
  (add-hook 'cider-mode-hook #'company-mode))

(use-package recentf
  :ensure t
  :init 
  (setq recentf-max-saved-items 50)
  :bind
  (("C-x C-r" . ido-recentf-open))
  :config
  (defun ido-recentf-open ()
    "Use `ido-completing-read' to \\[find-file] a recent file"
    (interactive)
    (if (find-file (ido-completing-read "Find recent file: " recentf-list))
	(message "Opening file...")
      (message "Aborting")))
  (recentf-mode t))

(use-package zerodark-theme
  :ensure t)
(use-package markdown-mode+
  :ensure t)
(use-package parinfer
  :ensure t)
(use-package exec-path-from-shell
  :ensure t)
(use-package gradle-mode
  :ensure t)
(use-package clojure-mode
  :ensure t)
(use-package ivy-todo
  :ensure t)
(use-package counsel
  :ensure t)


;;
;; miscellaneous settings
;;
(fset 'yes-or-no-p 'y-or-n-p)
(setq woman-use-own-frame nil) ;; don't create new frame for manpages
;; open *help* in current frame
(setq special-display-regexps (remove "[ ]?\\*[hH]elp.*" special-display-regexps))
;; enabling disabled commands.
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
;; handy aliases
(defalias 'fs'  toggle-frame-fullscreen)
(defalias 'qr'  query-replace)
(defalias 'qrr' query-replace-regexp)
(defalias 'adf' file-cache-add-directory-using-find)
(defalias 'adl' file-cache-add-directory-using-locate)

(set-language-environment "UTF-8")
(show-paren-mode t)
;; more emacs realestate
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

(defun rmws (&optional args)
  "removing trailing whitespaces and tabs while saving buffer"
  (interactive "p")
  (untabify (point-min) (point-max))
  (whitespace-cleanup)
  (save-buffer args))
(defun revert-buffer-custom (&optional ignore-auto preserve-modes)
  (interactive "P")
  (revert-buffer ignore-auto 'true preserve-modes))
(global-set-key "\C-c\C-r" 'revert-buffer-custom)

(setenv "JAVA_HOME" "/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home")
(setenv "PATH" (concat (getenv "JAVA_HOME") "/bin:"
		       "/Users/rratti/bin:"
		       "/usr/local/bin:"
		       (getenv "PATH")))
(add-to-list 'exec-path "/Users/rratti/bin" "/usr/local/bin")
      
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("1068ae7acf99967cc322831589497fee6fb430490147ca12ca7dd3e38d9b552a" "ff7625ad8aa2615eae96d6b4469fcc7d3d20b2e1ebc63b761a349bebbb9d23cb" default)))
 '(package-selected-packages
   (quote
    (org-mode use-package zerodark-theme markdown-mode+ parinfer exec-path-from-shell company cider magit gradle-mode clojure-mode ivy-todo fullscreen-mode counsel))))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Monaco" :height 150)))))
